# all imports should be in the beginning.
import telebot
from telebot import types

import psycopg2
import jsonpickle
import os
import json
import time
import zlib

# config
api_key = os.environ['aliasbot_api_key']
db_connection_string = os.environ['aliasbot_psycopg2_datastring']

# bot creating
bot = telebot.TeleBot(api_key)
conn = psycopg2.connect(db_connection_string)

class command_registry:
    def __init__(self):
        self.commands = {}

    def set_command(self, name, func):
        self.commands[name] = func

    def run_command(self, name, message):
        user_id = message.from_user.id

        cur = conn.cursor()
        cur.execute("select * from user_state where id=%s limit 1;", (user_id,))
        _, command, done, args_json = cur.fetchone()
        args = json.loads(args_json)
        state = (user_id, command, done, args, message)

        if (done == 0) and (self.commands.get(name) != None):
            self.commands[name](*state)

commands = command_registry()

def db_recreate():
    cur = conn.cursor()

    # table raw_log for keeping all incoming messages
    cur.execute("drop table if exists raw_log;")
    cur.execute("create table raw_log ( id bigint, chat_id bigint, msg_obj text );")

    # table user_state for keeping information about last command, called by user
    cur.execute("drop table if exists user_state;")
    cur.execute("create table user_state ( \
        id bigint, \
        command text, \
        done int, \
        args_json text, \
        primary key (id) );")

    # table quotes for keeping quotes
    cur.execute("drop table if exists quotes")
    cur.execute("create table quotes ( \
        user_id bigint, \
        name text, \
        value text \
        );")

    conn.commit()

#db_recreate()
#print("Database has been recreated")

def log(message):
    cur = conn.cursor()
    message_id = message.message_id
    chat_id = message.chat.id
    message_json = jsonpickle.encode(message)
    cur.execute("insert into raw_log (id, chat_id, msg_obj) values (%s, %s, %s)", (message_id, chat_id, message_json,))
    conn.commit()

def logged(fun):
    def fun2(message):
        log(message)
        fun(message)
    return fun2

def clear_state(user_id):
    cur = conn.cursor()
    cur.execute("update user_state set command='',done=0,args_json='[]' where id=%s;", (user_id,))
    conn.commit()

def ensure_user_state(user_id):
    cur = conn.cursor()
    cur.execute("select id from user_state where id=%s;", (user_id,))
    res = False
    for record in cur:
        selected_id = record[0]
        if selected_id == user_id:
            res = True

    if not res:
        command = ""
        done = 0
        args_json = "[]"
        to_insert = (user_id, command, done, args_json,)
        cur.execute("insert into user_state (id, command, done, args_json) \
            values (%s, %s, %s, %s);", to_insert)
        conn.commit()

def ensure_state(fun):
    def fun2(message):
        ensure_user_state(message.from_user.id)
        fun(message)
    return fun2

def state_action(message):
    user_id = message.from_user.id
    cur = conn.cursor()
    cur.execute("select * from user_state where id=%s limit 1;", (user_id,))
    _, command, done, args_json = cur.fetchone()
    commands.run_command(command, message)

def command_create(user_id, commands, done, args, message):
    args_json = json.dumps(args)
    to_send = "command handler works!" + \
        "\nargs: " + "\n" + args_json
    bot.reply_to(message, to_send)
    if (len(args) > 6):
        bot.reply_to(message, "Wow! Enough of arguments for me")
        done = 1
        cur = conn.cursor()
        to_insert = (done, user_id,)
        cur.execute("update user_state set done=%s where id=%s", to_insert)
        conn.commit()
        clear_state(user_id)

commands.set_command('create', command_create)

def command_quote(user_id, commands, done, args, message):
    argc = len(args)
    if (argc < 1):
        bot.reply_to(message, "Tell me the name of your quote.")
    elif (argc < 2):
        bot.reply_to(message, "Tell me the quote itself.")
    else:
        cur = conn.cursor()
        to_insert = (user_id, args[0], args[1],)
        cur.execute("insert into quotes (user_id, name, value) \
            values (%s, %s, %s);", to_insert)
        conn.commit()
        bot.reply_to(message, "Done! Quote saved.")
        clear_state(user_id)
        cur.execute("select value from quotes where user_id=%s limit 1002;", (user_id,))
        data = cur.fetchall()
        if len(data) > 1000:
            record = data[0]
            value = record[0]
            cur.execute("delete from quotes where (user_id=%s) and (value=%s);",\
                (user_id, value,))
            conn.commit()

commands.set_command('quote', command_quote)

def set_state_command(user_id, command):
    cur = conn.cursor()
    to_insert = (command, user_id)
    cur.execute("update user_state set command=%s where id=%s", to_insert)

@bot.message_handler(commands=['start', 'help'])
@logged
@ensure_state
def help(message):
    chat = message.chat.id
    bot.send_message(chat, "Hello! \nTo create quote, use /quote command and follow my instuctions. \nTo use the quote, use @yokalibot in inline mode")

@bot.message_handler(commands=['quote'])
@logged
@ensure_state
def quote(message):
    #print(message.from_user.username)
    clear_state(message.from_user.id)
    set_state_command(message.from_user.id, 'quote')
    state_action(message)

@bot.message_handler(commands=['create'])
@logged
@ensure_state
def create(message):
    bot.reply_to(message, 'Wow! It worked.')
    cur = conn.cursor()
    to_insert = ('create', message.from_user.id,)
    cur.execute("update user_state set command=%s where id=%s", to_insert)
    conn.commit()
    state_action(message)

@bot.message_handler(commands=['leavechat'])
@logged
@ensure_state
def leave(message):
    bot.leave_chat(message.chat.id)

@bot.message_handler(func = lambda m: True)
@logged
@ensure_state
def handle_arg(message):
    if hasattr(message, 'text'):
        arg = message.text
        cur = conn.cursor()
        user_id = message.from_user.id
        cur.execute("select * from user_state where id=%s limit 1;", (user_id,))
        _, command, done, args_json = cur.fetchone()
        args = json.loads(args_json)
        args.append(arg)
        args_json = json.dumps(args)

        to_insert = (args_json, user_id,)
        cur.execute("update user_state set args_json=%s where id=%s", to_insert)
        conn.commit()
    state_action(message)

@bot.inline_handler(lambda query: True)
def select_quote(inline_query):
    callback_id = inline_query.id
    text = inline_query.query

    cur = conn.cursor()
    text = '%' + text + '%'
    to_insert = (inline_query.from_user.id, text,)
    #print(to_insert)
    cur.execute("select * from quotes where \
        (user_id=%s) and \
        (name like %s) limit 50;",
        to_insert)
    results = []
    for result in cur:
        user_id, name, value = result
        uid = zlib.crc32(bytes(name + value, 'utf-8'))
        uid_string = str(user_id) + str(time.time()) + str(uid)

        item = types.InlineQueryResultArticle(uid_string, name, types.InputTextMessageContent(value))
        results.append(item)
    bot.answer_inline_query(callback_id, results, is_personal=True, cache_time=1, switch_pm_parameter="Y", switch_pm_text="Make new quote")


def do_polling():
    try:
        # do polling
        bot.polling()
    except:
        print("warning: some error occurred i feel.")
        do_polling()

def main():
    #do_polling()
    bot.polling() # use this replacing do_polling() if you want to see error information

main()