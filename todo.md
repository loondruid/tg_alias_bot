# todo in format that is close to todo.txt
# one line = one task with every piece of information about it, no difficult
# Man.

# build platform stack:
+install linux # ubuntu linux, but anything else should work mostly same, isn't it?
+install postgresql

# system access configuration:
+create\select user for bot
+generate password for bot user and save it to keepass
+create user for database same as user for bot
+generate password for database user for bot and save it to keepass

# table creation:
+create a database named same as bot
+delegate all right to this database for bot user

# structure creation:
+create table for keeping user messages from updates
+create table for keeping user state (last command, arguments, command was called already)
+create table for keeping aliases 


# here i found a little problem - i need to write code in my loved editor, so... Should probably use sftp for transfer, that will solve everything
install filezilla
connect to server with bot user's login\pwd

# some code:
create main executable script
create file describing dependencies
connect to database
test connection to database
connect to telegram
test if telegram is broken or it banned bot
make daemon-style initiator with disowning from parent process and user
react on command
log all received messages to db table
if a command issued, check that user has state in db
if user hasn't his state in db, create entry about his state in db

# commands:
command /makealias
arguments: alias name, alias value
logic: save alias into table with key, value, user(owner)

# inline interface:
when user starts typing, search his aliases by key and suggest possible solutions. 
when user clicks solution - output the text into user's message in selected chat

